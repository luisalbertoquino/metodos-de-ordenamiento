
package metodosdeordenamiento;

import javax.swing.JOptionPane;
import static metodosdeordenamiento.Burbuja.metodoOrdenacionBurbuja;

public class Quickshort {
    public void metodoQuickshort(){
    String b;
    int salir=1;
    int t=0;
   t=Integer.parseInt(JOptionPane.showInputDialog(null, "Digite la canidad de datos que va a ingresar"));
    int [] numeros=new int[t];
        
        
        do{ 
        
        try{
          b=JOptionPane.showInputDialog("a-Insertar datos\nb-Listar\nc-Ordenar\nd-Regresar");
               
          
               if(b.equals("a")){        
               for(int i = 0;i<t;i++){
               numeros[i]=Integer.parseInt(JOptionPane.showInputDialog(null, "ingrese numero"));
               }
          
              }if(b.equals("b")){
               for(int i = 0;i<t;i++){
               JOptionPane.showMessageDialog(null,numeros[i]);
               }
               
              }if(b.equals("c")){
                        int bucle=0;
                  do{
                         metodoOrdenacionQuickshort(numeros,0,numeros.length-1);
                         bucle++;
                  }while(bucle<3);
        
              }if(b.equals("d")){
        salir=0;
    }

        }catch(Exception ex){
           JOptionPane.showMessageDialog(null,"Ingrese a, b o c");
            }
    }while(salir==1);
    }
    
    
    public static void metodoOrdenacionQuickshort(int array[],int primero, int ultimo){
      int izquierda = primero;
      
      int derecha = ultimo;

      if (izquierda>= ultimo) {
          return;
      }
      int mitad = array[(izquierda + derecha) / 2];
      while (izquierda< derecha) {
          //haga esto mientras
          while (izquierda<derecha && array[izquierda] < mitad) {
              izquierda++;
          }
          while (izquierda<derecha && array[derecha] > mitad) {
              derecha--;
          }
          if (izquierda< derecha) {
              int T = array[izquierda];
              array[izquierda] = array[derecha];
              array[derecha] = T;
          }
      }
      if (derecha < izquierda) {
          int T = derecha;
          derecha = izquierda;
          izquierda = T;
      }
      metodoOrdenacionQuickshort(array, primero, izquierda);
      
   }
}
