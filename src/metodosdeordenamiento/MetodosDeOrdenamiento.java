
package metodosdeordenamiento;

import javax.swing.JOptionPane;

public class MetodosDeOrdenamiento {

    public static void main(String[] args) {
        int a;
        int salir=0;
        
        
        do{
            try{
                a=Integer.parseInt(JOptionPane.showInputDialog("Menu\n1-Ordenamiento por burbuja\n2-Ordenamiento Shell\n3-Ordenamiento QuickSort\n4-Ordenamiento de Arreglos\n5-Salir"));
        switch (a){
            case 1:
                Burbuja burbuja =new Burbuja();
                burbuja.metodoBurbuja();
                break;
            case 2:
                Shell shell =new Shell();
                shell.metodoShell();
                break;
            case 3:
                Quickshort quickshort =new Quickshort();
                quickshort.metodoQuickshort();
                break;
            case 4:
                Arreglos arreglos =new Arreglos();
                arreglos.metodoArreglos();
                break;
            case 5:
                salir=1;
                break;
            default:
                JOptionPane.showMessageDialog(null,"Ingrese un valor valido");
                break;
    }
            }catch(Exception ex){
           JOptionPane.showMessageDialog(null,"Digitar una opcion valida");
            }
        }while(salir==0);
    }
    
}


